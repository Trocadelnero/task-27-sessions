const express = require('express');
const session = require('express-session');
const app = express();

app.use(session({
    secret: "AskTheManOnTheKnoll",
    resave: false,
    saveUninitialized: false,
    cookie: {
      httpOnly: true,
      maxAge: 3600000
    }
}));

app.get("/", (req, res) => {
    if (!req.session.created == 1) {
        res.redirect(302,'/login');
    } else {
        res.redirect(302,'/dashboard');
    }
});

app.get('/create', (req, res) => {
  req.session.created = 1
  res.status(201).send("Session was created...");
});

app.get('/login', (req, res) => {
  res.status(200).send("Login Screen");
})

app.get('/dashboard', (req, res) => {
  res.status(200).send("Dashboard");
  console.dir(req.session);
})

//for random requests ;)
app.get("/*", (req, res) => {
  res.sendStatus(418);
})

app.listen(8080, () => console.log(`App live on port 8080...`));
