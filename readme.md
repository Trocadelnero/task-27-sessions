# Task 27 - User Session
---
- Create a basic express with four routes. 
- **No** HTML required. 
- Simply use the res.send('some text here');

### Dependencies

* "express": "^4.17.1",
*    "express-session": "^1.17.0"

- GET /
 Check if a user session exists, if it does, redirect to /dashboard if not, redirect to /login
- GET /create
 Create a random user session
- GET /login
 Show the text Login screen
- GET /dashboard
 Show the text Dashboard

 ## Authors
Jonah DelNero
---
## Acknowledgments

Inspiration, code snippets, etc.
* https://gist.github.com/HarshithaKP/3d9ad81138245aa7527bf385d37daba7

